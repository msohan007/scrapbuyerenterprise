﻿SELECT * FROM Users;
SELECT * FROM Role_Master;
SELECT * FROM Bill_Details;
SELECT * FROM Buy_Scarp;
SELECT * FROM Sell_Scrap;
SELECT * FROM Scrap_Stock;

DROP TABLE users;
DROP TABLE Role_Master;
DROP TABLE Bill_Details;
DROP TABLE Buy_Scarp;


CREATE TABLE Users(
id SERIAL,
fname	VARCHAR(50),
lname	VARCHAR(50),
email	VARCHAR(30),
phone int8,
role int,
address	VARCHAR(150),
username	VARCHAR(20),
password	VARCHAR(20),
request_date timestamp DEFAULT CURRENT_TIMESTAMP,
status VARCHAR(20),
approve_date timestamp,
PRIMARY KEY (id));


CREATE TABLE Role_Master(
id SERIAL,
details VARCHAR(20),
PRIMARY KEY (id));


CREATE TABLE Bill_Details(
id SERIAL,
customer_id int,
total_amount decimal (6, 2),
payment_status int,
bill_category VARCHAR(30),
PRIMARY KEY (id));


CREATE TABLE Buy_Scrap(
id SERIAL,
customer_id int,
product_id int,
quantity decimal (4, 3),
total_price decimal (6, 3),
date timestamp DEFAULT CURRENT_TIMESTAMP,
staus int,
PRIMARY KEY (id));


CREATE TABLE Scrap_Products(
id SERIAL,
name VARCHAR(50),
unit float4,
price float4,
date timestamp,
PRIMARY KEY (id));


CREATE TABLE Scrap_Stock(
id SERIAL,
product_id int,
total_quantity decimal (6, 3),
PRIMARY KEY (id));


CREATE TABLE Sell_Scrap(
id SERIAL,
customer_id int,
product_id int,
quantity decimal (6, 3),
price decimal (6, 3),
date timestamp DEFAULT CURRENT_TIMESTAMP,
status int,
PRIMARY KEY (id));

