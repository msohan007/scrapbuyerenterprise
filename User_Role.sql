﻿CREATE TABLE User_Role(
id int,
name VARCHAR(20),
PRIMARY KEY(id));


SELECT * FROM User_Role;

INSERT INTO User_Role(id,name) 
VALUES (1,'Employee'),
(2,'Admin'),
(3,'Buyer'),
(4,'Seller');
