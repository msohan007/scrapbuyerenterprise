<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from malikhassan.com/html/construxive/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Feb 2021 18:02:25 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Raihan Basanalaya</title>

<!-- Fav Icon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Header start -->
<div class="navbar custom-navbar wow fadeInDown" data-wow-duration="2s" role="navigation" id="header">
  <div class="header">
    <div class="container"> 
      <!-- Row Rtart -->
      <div class="row">
        <div class="col-md-3 col-sm-12">
          <div class="logo"><img src="images/logo.png" alt=""></div>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
        </div>
        <div class="col-md-9 col-sm-12"> 
          <!--navegation-->
          <div class="navigationwrape">
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                  <li> <a href="#home" class="smoothScroll"> Home</a></li>
                  <li> <a href="#about" class="smoothScroll"> About Us</a></li>
                  <li> <a href="#service" class="smoothScroll"> Our Products</a></li>
                  <!-- <li> <a href="#porfolio" class="smoothScroll"> porfolio</a></li> -->
                  <!-- <li> <a href="#testimonials" class="smoothScroll"> Testimonials </a></li> -->
                  <li> <a href="#team" class="smoothScroll"> Team</a></li>
                  <li> <a href="#contact" class="smoothScroll"> Contact us </a></li>
                  <li>
                    <div class="phone"> <span>Contact Us Today!</span> <a href="#">+1 (514) 946-1017</a> </div>
                  </li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
      <!-- Row End --> 
    </div>
  </div>
</div>
<!-- Header end --> 

<!-- slider start -->
<div  style="background-image:url('images/background.jpg');background-size: cover;" id="home">
  <div class="container">
    <div class="banner-info">
      <h2>Welcome to <span style="color:red">Raihan Basanalaya</span></h2>
      <p>If you are looking for good price for your recyleable scrap materials 
        you're at right place</p>
      <div class="readmore"><a href="admin/loginForm.php">Login</a></div>
    </div>
  </div>
</div>

<!-- slider end --> 

<!--About Start-->
<div class="about-wrap" id="about">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>About <span>Our Company</span></h1>
        <div class="aboutTxt">We are buying recyclabe materials and selling new utensils</div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non libero consectetur, blandit mauris eget, imperdiet nisl. Etiam commodo ex nec erat tempor varius. Nunc rutrum pretium nunc in malesuada. Curabitur mollis urna ac sapien vulputate, ut congue sapien vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam et suscipit dui. </p>
        <div class="about-types row">
          <div class="col-md-6 col-sm-6 p-tb15 ">
            <div class="wt-icon-box-wraper left">
              <div class="aboutIcon"> <i class="fa fa-paint-brush" aria-hidden="true"></i> </div>
              <div class="icon-content">
                <h5 class="wt-tilte text-uppercase m-b0">Recyclin</h5>
                <p>Lorem ipsum dolor sit piscing sed diam nonmy end .</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 p-tb15">
            <div class="wt-icon-box-wraper left">
              <div class="aboutIcon"> <i class="fa fa-picture-o" aria-hidden="true"></i> </div>
              <div class="icon-content">
                <h5 class="wt-tilte text-uppercase m-b0">Financial benifits for resident</h5>
                <p>Lorem ipsum dolor sit piscing sed diam nonmy end .</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 p-tb15">
            <div class="wt-icon-box-wraper left">
              <div class="aboutIcon"> <i class="fa fa-building" aria-hidden="true"></i> </div>
              <div class="icon-content">
                <h5 class="wt-tilte text-uppercase m-b0">Selling utensils</h5>
                <p>Lorem ipsum dolor sit piscing sed diam nonmy end .</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 p-tb15">
            <div class="wt-icon-box-wraper left">
              <div class="aboutIcon"> <i class="fa fa-gavel" aria-hidden="true"></i> </div>
              <div class="icon-content">
                <h5 class="wt-tilte text-uppercase m-b0 ">Contribute to society</h5>
                <p>Lorem ipsum dolor sit piscing sed diam nonmy end .</p>
              </div>
            </div>
          </div>
        </div>
        <div class="readmore"><a href="#">Read More</a></div>
      </div>
      <div class="col-md-5">
        <div class="about-image"><img src="images/about.jpg"></div>
      </div>
    </div>
  </div>
</div>
<!--About End--> 

<!--Quote Start-->
<div class="quote-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2>Ready to become a partner?</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in condimentum risus. Fusce rutrum, leo in elementum sodales, magna eros vehicula ante, eget semper mi lectus nec ipsum.</p>
      </div>
      <div class="col-md-3">
        <div class="quote-btn"><a href="qoute.html">Get a Quote</a></div>
      </div>
    </div>
  </div>
</div>
<!--Quote End--> 

<!--service start-->
<div class="service-wrap" id="service">
  <div class="container">
    <h1>Our <span>Goal</span></h1>
    <ul class="serviceList row">
      <li class="col-md-4 col-sm-6">
        <div class="single-service">
          <div class="service-icon"><i class="fa fa-home" aria-hidden="true"></i></div>
          <h3><a href="#">Clean Environment</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in leo vitae nisi rutrum fringilla. Nunc consequat massa sed porta convallis. Sed quis sollicitudin risus. Morbi vitae semper dolor.</p>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="single-service">
          <div class="service-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
          <h3><a href="#">Financial benifitter</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in leo vitae nisi rutrum fringilla. Nunc consequat massa sed porta convallis. Sed quis sollicitudin risus. Morbi vitae semper dolor.</p>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="single-service">
          <div class="service-icon"><i class="fa fa-building-o" aria-hidden="true"></i></div>
          <h3><a href="#">Be a part of sustainable development activity</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in leo vitae nisi rutrum fringilla. Nunc consequat massa sed porta convallis.</p>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="single-service">
          <div class="service-icon"><i class="fa fa-hospital-o" aria-hidden="true"></i></div>
          <h3><a href="#">Trustworthy partnership</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in leo vitae nisi rutrum fringilla. Nunc consequat massa sed porta convallis. Sed quis sollicitudin risus. Morbi vitae semper dolor.</p>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="single-service">
          <div class="service-icon"><i class="fa fa-wrench" aria-hidden="true"></i></div>
          <h3><a href="#">Garranty for product quality</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in leo vitae nisi rutrum fringilla. Nunc consequat massa sed porta convallis. Sed quis sollicitudin risus. Morbi vitae semper dolor.</p>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="single-service">
          <div class="service-icon"><i class="fa fa-home" aria-hidden="true"></i></div>
          <h3><a href="#">Friendly service</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in leo vitae nisi rutrum fringilla. Nunc consequat massa sed porta convallis. Sed quis sollicitudin risus. Morbi vitae semper dolor.</p>
        </div>
      </li>
    </ul>
  </div>
</div>
<!--service end--> 

<!--Counter Start-->
<div id="counter">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
          <span class="counter-number" data-from="1" data-to="399" data-speed="1000">399</span> <span class="counter-text">Happy Client</span> </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-code" aria-hidden="true"></i></div>
          <span class="counter-number" data-from="1" data-to="8312" data-speed="2000">8312</span> <span class="counter-text">Code Line</span> </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-laptop" aria-hidden="true"></i></div>
          <span class="counter-number" data-from="1" data-to="1632" data-speed="3000">1632</span> <span class="counter-text">Project Finished</span> </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
        <div class="counterbox">
          <div class="counter-icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>
          <span class="counter-number" data-from="1" data-to="206" data-speed="4000">206</span> <span class="counter-text">Awards</span> </div>
      </div>
    </div>
  </div>
</div>
<!--Counter End--> 

<!--porfolio start-->
<div class="porfolio-wrap" id="porfolio">
  <div class="container">
    <h1>Buying <span>Recyclable Products</span></h1>
    <ul class="row portfolio-service">
      <li class="col-md-4 col-sm-6">
        <div class="project-image" ><img style="width: 400px;height: 400px;" src="images/scrapiron.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Iron</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/scrapplastic.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Plastic</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image" ><img style="width: 400px;height: 400px;" src="images/scrapled.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Led</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/scrapcopper.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Copper</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/scrapaluminium.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Aluminium</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/scrappriston.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Priston</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
<!--******************************** SELLING PRODUCTS ------------------------>
<div class="porfolio-wrap" id="porfolio">
  <div class="container">
    <h1>Selling <span>Utensils</span></h1>
    <ul class="row portfolio-service">
      <li class="col-md-4 col-sm-6">
        <div class="project-image" ><img style="width: 400px;height: 400px;" src="images/spoon.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Spoon</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/cookingcontainer.png">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Cooking Pot</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image" ><img style="width: 400px;height: 400px;" src="images/pan.webp">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Frying Pan</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/copperplate.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Copper Plate</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/stainlessplate.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Stainless Steel Plate</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
      <li class="col-md-4 col-sm-6">
        <div class="project-image"><img style="width: 400px;height: 400px;" src="images/biodegradableplate.jpg">
          <div class="portfolio-overley">
            <div class="content">
              <h3><a href="#">Biodegradable Plate</a></h3>
              <div class="portfolio-tags"> <span>Builder</span>, <span>Repairman</span> </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
<!--porfolio end--> 

<!--Testimonials start-->
<div class="testimonials-wrap" id="testimonials">
  <div class="container">
    <div class="heading-wrap">
      <h1>Testimonials</h1>
    </div>
    <ul class="owl-carousel testimonials">
      <li class="item">
        <div class="testService">
          <div class="testi-info">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis augue ultricies, molestie nisl mollis, efficitur velit. Nunc urna ligula, malesuada nec condimentum eu, tincidunt sit amet purus.</p>
          </div>
          <div class="client-image"><img src="images/client.jpg" alt=""></div>
          <div class="name">John Doe <span>Lorem Ispum</span></div>
        </div>
      </li>
      <li class="item">
        <div class="testService">
          <div class="testi-info">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis augue ultricies, molestie nisl mollis, efficitur velit. Nunc urna ligula, malesuada nec condimentum eu, tincidunt sit amet purus.</p>
          </div>
          <div class="client-image"><img src="images/client2.jpg" alt=""></div>
          <div class="name">John Doe <span>Lorem Ispum</span></div>
        </div>
      </li>
      <li class="item">
        <div class="testService">
          <div class="testi-info">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis augue ultricies, molestie nisl mollis, efficitur velit. Nunc urna ligula, malesuada nec condimentum eu, tincidunt sit amet purus.</p>
          </div>
          <div class="client-image"><img src="images/client.jpg" alt=""></div>
          <div class="name">John Doe <span>Lorem Ispum</span></div>
        </div>
      </li>
      <li class="item">
        <div class="testService">
          <div class="testi-info">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis augue ultricies, molestie nisl mollis, efficitur velit. Nunc urna ligula, malesuada nec condimentum eu, tincidunt sit amet purus.</p>
          </div>
          <div class="client-image"><img src="images/client2.jpg" alt=""></div>
          <div class="name">John Doe <span>Lorem Ispum</span></div>
        </div>
      </li>
      <li class="item">
        <div class="testService">
          <div class="testi-info">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis augue ultricies, molestie nisl mollis, efficitur velit. Nunc urna ligula, malesuada nec condimentum eu, tincidunt sit amet purus.</p>
          </div>
          <div class="client-image"><img src="images/client.jpg" alt=""></div>
          <div class="name">John Doe <span>Lorem Ispum</span></div>
        </div>
      </li>
      <li class="item">
        <div class="testService">
          <div class="testi-info">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis augue ultricies, molestie nisl mollis, efficitur velit. Nunc urna ligula, malesuada nec condimentum eu, tincidunt sit amet purus.</p>
          </div>
          <div class="client-image"><img src="images/client2.jpg" alt=""></div>
          <div class="name">John Doe <span>Lorem Ispum</span></div>
        </div>
      </li>
    </ul>
  </div>
</div>
<!--Testimonials end--> 

<!--Team Start-->
<div id="team">
  <div class="team_wrap">
    <div class="container">
      <h1>OUR <span>Family</span></h1>
      <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br>
        tetuer adipis elit, aliquam eget nibh etlibura.</p>
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="team">
            <div class="team-image"> <img src="images/team01.png" alt="">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <div class="team-info">
              <h5><a href="#">MR. HOCHEN SEIKH</a></h5>
              <span>Owner</span> </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="team">
            <div class="team-image"> <img src="images/team02.png" alt="">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <div class="team-info">
              <h5><a href="#">Mr. ABDUS SEIKH</a></h5>
              <span>Supervisor</span> </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="team">
            <div class="team-image"> <img src="images/team03.png" alt="">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <div class="team-info">
              <h5><a href="#">MR. AHID SEIKH</a></h5>
              <span>Employee</span> </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="team">
            <div class="team-image"> <img src="images/team04.png" alt="">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <div class="team-info">
              <h5><a href="#">MR. ABAIDULLAH SEIKH</a></h5>
              <span>Employee</span> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Team end--> 

<!--Contact Us Start-->
<div class="contact-wrap" id="contact">
  <div class="container">
    <div class="contact-info">
      <div class="title">
        <h1>Send Us A Message</h1>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias dicta ratione expedita Duis gravida arcu lectus, porttitor placerat elit pharetra a. Quisque id tellus risus. Nam eget gravida tortor.</p>
      <form>
        <div class="row">
          <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control" name="name" placeholder="Name">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control" name="email" placeholder="Email">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control" name="phone" placeholder="Phone">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control" name="address" placeholder="Address">
            </div>
          </div>
          <div class="col-lg-12">
            <div class="input-group">
              <textarea class="form-control" placeholder="Message"></textarea>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="contact-btn">
              <input type="submit" value="Submit Now" class="sub">
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Contact Us end--> 

<!--Footer Start-->
<div class="footer-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6">
        <div class="copyright">©Copyright 2020 Raihan Basanalaya. All Rights Reserved</div>
      </div>
      <div class="col-md-8  col-sm-6">
        <ul class="social-icons">
          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--Footer end--> 

<!--page scroll start-->
<div class="page-scroll scrollToTop"><a href="#"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></div>
<!--page scroll start--> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery-2.1.4.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script> 

<!-- general script file --> 
<script src="js/owl.carousel.js"></script> 

<!-- sticky script file --> 
<script src="js/jquery.sticky.js"></script> 

<!-- Counter --> 
<script src="js/counter.js"></script> 

<!-- smoothscroll script file --> 
<script src="js/smoothscroll.js"></script> 

<!-- custom script file --> 
<script src="js/custom.js"></script> 
<script src="js/script.js"></script>
</body>

<!-- Mirrored from malikhassan.com/html/construxive/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Feb 2021 18:02:50 GMT -->
</html>