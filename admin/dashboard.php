﻿<?php
// Start the session
session_start();
// check whether session variable exists
if (isset($_SESSION['user_name'])) {
    //echo '<p align="center" style="color:green; font-size:36px"> Please login first before you can visit this page! </p>';
    //header("refresh:3, url=./loginForm.php");
    //die();
} else {
    echo "<script>location.href='loginForm.php'</script>";
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<?php include('includes/layout/header.php'); ?>
<?php include('includes/layout/left-sidebar.php'); ?>


<!--PAGE CONTENT -->
<div id="content">

    <div class="inner" style="min-height: 700px;">
        <div class="row">
            <div class="col-lg-12">
                <h1> Admin Dashboard </h1>
            </div>
        </div>
        <hr />
        <!--BLOCK SECTION -->
        <div class="row">
            <div class="col-lg-12">
                <div style="text-align: center;">
                    <a class="quick-btn" href="products/scrap-products.php">
                        <i class="icon-check icon-2x"></i>
                        <span>Scrap Prod.</span>
                        <span class="label label-danger">2</span>
                    </a>

                    <a class="quick-btn" href="products/new-products.php">
                        <i class="icon-bolt icon-2x"></i>
                        <span>New Prod.</span>
                        <span class="label label-default">20</span>
                    </a>
                    <a class="quick-btn" href="#">
                        <i class="icon-envelope icon-2x"></i>
                        <span>Messages</span>
                        <span class="label label-success">456</span>
                    </a>
                    <a class="quick-btn" href="#">
                        <i class="icon-signal icon-2x"></i>
                        <span>Profit</span>
                        <span class="label label-warning">+25</span>
                    </a>
                    <a class="quick-btn" href="#">
                        <i class="icon-external-link icon-2x"></i>
                        <span>Update Price</span>
                        <span class="label btn-metis-2">3.14159265</span>
                    </a>
                    <a class="quick-btn" target="_blank" href="/admin/user-management.php">
                        <i class="icon-lemon icon-2x"></i>
                        <span>Task</span>
                        <span class="label btn-metis-4">107</span>
                    </a>
                </div>
            </div>

        </div>
        <!--END BLOCK SECTION -->
        <hr />
        <!-- CHART & CHAT  SECTION -->
        <div class="row">
            <div id="display" class="container bg-body col-lg-12 ">

            </div>
        </div>
        <!--END CHAT & CHAT SECTION -->
    </div>

</div>
<!--END PAGE CONTENT -->

<!-- RIGHT STRIP  SECTION -->
<?php include('includes/layout/right-sidebar.php'); ?>

</div>
<!--END MAIN WRAPPER -->

<!-- FOOTER SECTION -->
<?php include('includes/layout/footer.php'); ?>


<!-- GLOBAL SCRIPTS -->
<script src="assets/plugins/jquery-2.0.3.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->

<!-- PAGE LEVEL SCRIPTS -->
<script src="assets/plugins/flot/jquery.flot.js"></script>
<script src="assets/plugins/flot/jquery.flot.resize.js"></script>
<script src="assets/plugins/flot/jquery.flot.time.js"></script>
<script src="assets/plugins/flot/jquery.flot.stack.js"></script>
<script src="assets/js/for_index.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->

</html>