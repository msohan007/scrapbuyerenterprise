<?php
// Start session
session_start();

if (isset($_SESSION['user_name'])) {
    // Destroy the session
    session_destroy();
    echo '<p align="center" style="color:green; font-size:36px"> Successfully logged out! </p>';
    header("refresh:2, url=./../index.php");
    die();
}else{
    echo "<script>location.href='loginForm.php'</script>";
}
