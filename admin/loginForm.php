﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>
    <meta charset="UTF-8" />
    <title>BCORE Admin Dashboard Template | Login Page</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/css/login.css" />
    <link rel="stylesheet" href="assets/plugins/magic/magic.css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body style="background-color: gray;">

    <!-- PAGE CONTENT -->
    <div class="container" >
        <div class="text-center">
        <h2 style="font-weight:bold; border-bottom:yellow solid 3px;padding:1%">WELCOME <br/>TO <br/><span style="color:burlywood">RAIHAN BASANALAYA</span></h2>
            <!-- <img style="margin-right: 50px;" src="assets/img/seikh_logo.png" id="logoimg" alt=" Logo" /> -->
        </div>
        <div class="tab-content">
            <div id="login" class="tab-pane active">

                <form method="POST" action="login.php" class="form-signin">
                    <p class="text-muted text-center btn-block btn btn-primary btn-rect">
                        Enter your username and password
                    </p>
                    <input type="text" name="user" placeholder="Username" class="form-control" />
                    <input type="password" name="pwd" placeholder="Password" class="form-control" />
                    <button class="btn text-muted text-center btn-danger" type="submit">Sign in</button>
                </form>
            </div>
            <div id="forgot" class="tab-pane">
                <form action="index.html" class="form-signin">
                    <p class="text-muted text-center btn-block btn btn-primary btn-rect">Enter your valid e-mail</p>
                    <input type="email" required="required" placeholder="Your E-mail" class="form-control" />
                    <br />
                    <button class="btn text-muted text-center btn-success" type="submit">Recover Password</button>
                </form>
            </div>

            <div id="signup" class="tab-pane">
                <form method="POST" action="register.php" class="form-signin">
                    <p class="text-muted text-center btn-block btn btn-primary btn-rect">Please Fill Details To Register</p>
                    <input type="text" name="fname" placeholder="First Name" class="form-control" />
                    <input type="text" name="lname" placeholder="Last Name" class="form-control" />
                    <input type="email" name="email" placeholder="Your E-mail" class="form-control" />
                    <input style="margin-top:10px" type="number" name="phone" placeholder="Your phone" class="form-control" />
                    <input type="text" name="address" placeholder="Your address" class="form-control" />
                    <!--<input type="text" name="role" placeholder="Your role" class="form-control" />-->

                    <select class="form-control" type='text' name='role' style="margin-top:10px">
                        <option value="0">Select role</option>
                        <?php
                        include_once('./../db_connection.php');
                        //Bring roles to the register form
                        $getRoles = pg_query($conn, "SELECT id, details FROM Role_Master");
                       
                        while ($row = pg_fetch_assoc($getRoles)) {
                            echo "<option value='$row[id]'>$row[details]</option>";                                                     
                        }
                        ?>
                    </select>
                    <input type="text" name="username" placeholder="Username" class="form-control" />
                    <input type="password" name="password" placeholder="password" class="form-control" />
                    <input type="password" name="password2" placeholder="Re type password" class="form-control" />
                    <button class="btn text-muted text-center btn-success" type="submit">Register</button>
                </form>
            </div>
        </div>
        <div class="text-center">
            <ul class="list-inline">
                <li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>
                <li><a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a></li>
                <li><a class="text-muted" href="#signup" data-toggle="tab">Signup</a></li>
            </ul>
        </div>


    </div>

    <!--END PAGE CONTENT -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="assets/plugins/jquery-2.0.3.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="assets/js/login.js"></script>
    <!--END PAGE LEVEL SCRIPTS -->

</body>
<!-- END BODY -->

</html>