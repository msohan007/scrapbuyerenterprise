<?php
// Start the session
session_start();
// check whether session variable exists
if (isset($_SESSION['user_name'])) {
    //echo '<p align="center" style="color:green; font-size:36px"> Please login first before you can visit this page! </p>';
    //header("refresh:3, url=./loginForm.php");
    //die();
} else {
    echo "<script>location.href='loginForm.php'</script>";
}
?>

<?php include_once('../../../db_connection.php'); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>
    <meta charset="UTF-8" />
    <title>Product-update</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../assets/css/main.css" />
    <link rel="stylesheet" href="../../assets/css/theme.css" />
    <link rel="stylesheet" href="../../assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="../../assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="../../assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!-- END PAGE LEVEL  STYLES -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->
    <!--Jquery-->
    <script src="jquery-3.5.1.min.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="padTop53 ">

    <!-- MAIN WRAPPER -->
    <div id="wrap">
        <?php include('../layout/header.php'); ?>
        <!-- LEFT SIDEBAR SECTION -->
        <?php include('../layout/left-sidebar.php'); ?>
        <!--END LEFT SIDEBAR SECTION -->

        <!--PAGE CONTENT -->
        <div id="content">
            <div class="inner">
                <div class="row">
                    <div class="col-lg-12">
                        <h2> Update Product </h2>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Product List Table
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Product Name</th>
                                                <th>Unit (Kg/Pc)</th>
                                                <th>Price</th>
                                                <th>Last Changed</th>
                                                <th>Status</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            //Bring roles to the register form
                                            $getProducts = pg_query($conn, "SELECT id,name,unit,price,date,status FROM scrap_products");
                                            while ($prod = pg_fetch_assoc($getProducts)) { ?>
                                                <tr class="odd gradeX">
                                                    <form method='POST'>
                                                        <input type="hidden" name="id" id="id" value="<?php echo $row['id']; ?>">
                                                        <td name='id'><?php echo $prod['id']; ?></td>
                                                        <td name='name'><?php echo $prod['name']; ?></td>
                                                        <td name='unit'><?php echo $prod['unit']; ?></td>
                                                        <td name='price'><?php echo $prod['price']; ?></td>
                                                        <td name='date'><?php echo $prod['date']; ?></td>
                                                        <td name='status'><?php echo $prod['status']; ?> </td>
                                                        <!-- <td><?php echo $row['request_date']; ?></td> -->
                                                        <td>
                                                            <button onClick="infoAlert()" class='btn btn-info'><i class="icon-info"></i></button>
                                                            <script>
                                                                function infoAlert() {
                                                                    alert("This user has 500$ debt")
                                                                }
                                                            </script>
                                                        </td>
                                                        <td>
                                                            <a data-toggle="modal" data-id="<?php echo $row['id'] . ':' . $row['name'] . ':' . $row['unit'] . ':' . $row['price'] . ':' . $row['date'] . ':' . $row['status']; ?>" class="passingID">
                                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formModal">
                                                                    <i class="fas fa-pencil-alt"></i> Edit</button>
                                                            </a>
                                                        </td>
                                                        <td><button type='submit' name="delete" class='btn btn-danger'>Delete</button></td>



                                                </tr>
                                                </form>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                        <?php
                                        /**  DELETE Method */
                                        if (isset($_POST['delete'])) {
                                            $varId = "";
                                            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                                $ProdId = $_POST["id"];
                                                //echo $varUser = $_POST["id"]; exit;
                                            }
                                            $deleteProduct = pg_query($conn, "DELETE FROM scrap_products WHERE id='" . $ProdId . "'");
                                            echo "<script>location.href='../products/update-products.php''</script>";
                                        }
                                        ?>

                                        <?php
                                        function role_description($id)
                                        {
                                            global $conn;
                                            $roleName = pg_query($conn, "SELECT details FROM role_master WHERE id='" . $id . "'");
                                            $role_name = pg_fetch_array($roleName);
                                            return $role_name['details'];
                                        }

                                        //{
                                        //     $varUserId = "";
                                        //     $varUserId = $_POST["id"];
                                        //     //echo $varUser = $_POST["id"]; exit;

                                        //     $getUser = pg_query($conn, "SELECT * FROM Users WHERE id =3");
                                        //     //$getUser = pg_query($conn, "SELECT * FROM Users WHERE id ='" + $userId + "'");
                                        //     while ($edit = pg_fetch_assoc($getUser)) {
                                        //         $fname = $edit["fname"];
                                        //         $lname = $edit["lname"];
                                        //         $phone = $edit["phone"];
                                        //         $address = $edit["address"];
                                        //         $role = $edit["role"];

                                        //         //echo $varUser = $_POST["id"]; exit;
                                        //     }
                                        // }
                                        ?>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
        </div>
        <!--END PAGE CONTENT -->

        <!-- RIGHT STRIP  SECTION -->
        <?php include('../layout/right-sidebar.php'); ?>
    </div>
    <!--END MAIN WRAPPER -->
    <!-------MODAL SEction HERE Start------>

    <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="H2">Update User</h4>
                </div>
                <div class="modal-body">
                    <form method='POST' action="update-product.php">
                        <!-- <input type="hidden" name="test" id="test" value="" /> -->
                        <div class="form-group">
                            <label>Product Name </label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Update name" value="<?php echo  $name; ?>" />
                        </div>
                        <div class="form-group">
                            <label>Unit </label>
                            <input type="number" name="unit" id="unit" class="form-control" placeholder="Update unit" value="<?php echo  $unit; ?>" />
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" name="price" id="price" class="form-control" placeholder="Update price" value="<?php echo  $price; ?>" />
                        </div>
                        <div class="form-group">
                        <?php $sys_date=TIME()?>
                            <label>Date</label>
                            <input type="text" name="date" id="address" class="form-control" placeholder="Update date" value="<?php  echo $sys_date ; ?>" />
                        </div>
                        <div class="form-group">
                            <!-- <select class="form-control" type='text' name='role' style="margin-top:10px">
                                <option value="0">Select role</option>
                                <?php
                                $getRoles = pg_query($conn, "SELECT id, details FROM Role_Master");
                                while ($row = pg_fetch_assoc($getRoles)) {
                                    echo "<option value='$row[id]'>$row[details]</option>";
                                }
                                ?>
                            </select> -->
                            <label>Status</label>
                            <input type="text" name="status" id="status" class="form-control" placeholder="Update status" value="<?php echo  $status; ?>" />
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button name="update-submit" type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
                <?php

                // data (pre)processing
                function test_input($data)
                {
                    $data = trim($data); // Strip whitespace (or other characters)
                    $data = stripslashes($data); // Return a string with backslashes stripped off and un-quote the quoted string
                    $data = htmlspecialchars($data); // Convert special characters to html entity
                    return $data;
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    $id = $FirstName = $LastName =  $Phone = $Address = $Role =  ""; // set variables to empty

    if (isset($_POST['update-submit'])) {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $Id = test_input($_POST["test"]);
            $FirstName = test_input($_POST["fname"]);
            $LastName = test_input($_POST["lname"]);
            $Phone = test_input($_POST["phone"]);
            $Address = test_input($_POST["address"]);
            $Role = test_input($_POST["role"]);
        }
        $sql = "UPDATE Users SET fname = '" . $FirstName . "', lname = '" . $LastName . "', phone = '" . $Phone . "',  address = '" . $Address . "', role = '" . $Role . "' WHERE id='" . $Id . "'";
        $updateUser = pg_query($conn, $sql);
    }
    ?>

    <!---Modal End here---->




    <!-- FOOTER -->
    <?php include('../layout/footer.php'); ?>
    <!--END FOOTER -->



    <!-- GLOBAL SCRIPTS -->
    <script src="../../assets/plugins/jquery-2.0.3.min.js"></script>
    <!-- <script src="assets/plugins/jquery-2.0.3.min.js"></script> -->
    <script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="../../assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
    <!-- END GLOBAL SCRIPTS -->
    <!-- PAGE LEVEL SCRIPTS -->
    <script src="../../assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });

        $(".passingID").click(function() {
            //alert('kkkk');
            var val = $(this).attr('data-id');
            //alert(val);
            var res = val.split(":");

            //alert(res[0]);
            $("#test").val(res[0]);
            $("#fname").val(res[1]);
            $("#lname").val(res[2]);
            $("#phone").val(res[3]);
            $("#address").val(res[4]);
            $("#role").val(res[5]);
            $('#myModal').modal('show');
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->

</html>