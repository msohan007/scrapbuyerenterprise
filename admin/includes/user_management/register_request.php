<?php
// Start the session
session_start();
// check whether session variable exists
if (isset($_SESSION['user_name'])) {
    //echo '<p align="center" style="color:green; font-size:36px"> Please login first before you can visit this page! </p>';
    //header("refresh:3, url=./loginForm.php");
    //die();
} else {
    echo "<script>location.href='loginForm.php'</script>";
}
?>

<?php include_once('../../../db_connection.php'); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>
    <meta charset="UTF-8" />
    <title>Register-request</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../assets/css/main.css" />
    <link rel="stylesheet" href="../../assets/css/theme.css" />
    <link rel="stylesheet" href="../../assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="../../assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="../../assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!-- END PAGE LEVEL  STYLES -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="padTop53 ">

    <!-- MAIN WRAPPER -->
    <div id="wrap">

        <?php include('../layout/header.php'); ?>

        <!-- LEFT SIDEBAR SECTION -->
        <?php include('../layout/left-sidebar.php'); ?>
        <!--END LEFT SIDEBAR SECTION -->


        <!--PAGE CONTENT -->
        <div id="content">
            <div class="inner">
                <div class="row">
                    <div class="col-lg-12">
                        <h2> Registration Request </h2>
                    </div>
                </div>
                <hr />


                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                List of Reigistration Request
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th>Role</th>
                                                <th>Date</th>
                                                <th>Assign Role</th>
                                                <th>Action-1</th>
                                                <th>Action-2</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            //Bring roles to the register form
                                            $getUsers = pg_query($conn, "SELECT id,fname,lname,email,phone,address,role,request_date,username,password FROM Users WHERE status IS NULL");
                                            while ($row = pg_fetch_assoc($getUsers)) { ?>
                                                <tr class="odd gradeX">
                                                    <form method='POST'>
                                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                        <td name='id'><?php echo $row['id']; ?></td>
                                                        <td><?php echo $row['fname']; ?></td>
                                                        <td><?php echo $row['lname']; ?></td>
                                                        <td><?php echo $row['phone']; ?></td>
                                                        <td><?php echo $row['address']; ?></td>
                                                        <td><?php echo $row['role']; ?></td>
                                                        <td><?php echo $row['request_date']; ?></td>
                                                        <td>
                                                            <select name='asgn_role'>
                                                                <?php
                                                                //Bring roles to the register form
                                                                $getRole = pg_query($conn, "SELECT * FROM role_master");
                                                                while ($rowrole = pg_fetch_assoc($getRole)) { ?>

                                                                    <option value='<?php echo $rowrole['id']; ?>'><?php echo $rowrole['details']; ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td><button type='submit' name="approv" class='btn btn-primary'>Approve</button></td>
                                                        <td><button type='submit' name="reject" class='btn btn-danger'>Reject</button></td>
                                                    </form>
                                                </tr>

                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                        <?php
                                        if (isset($_POST['approv'])) {
                                            $varId = $varAssignRole = "";
                                            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                                $UserId = $_POST["id"];
                                                //$varAssignRole = $_POST["asgn_role"];
                                            }
                                            $assignRole = pg_query($conn, "UPDATE Users SET status='1' WHERE id='" . $UserId . "'");
                                        }

                                        if (isset($_POST['reject'])) {
                                            $varId =  "";
                                            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                                $UserId = $_POST["id"];
                                                //echo $varUser = $_POST["id"]; exit;
                                            }
                                            $assignRole = pg_query($conn, "DELETE FROM Users WHERE id='" . $UserId . "'");
                                        }

                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END PAGE CONTENT -->


        </div>
        <!--END PAGE CONTENT -->

        <!-- RIGHT STRIP  SECTION -->
        <?php include('../layout/right-sidebar.php'); ?>
    </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <?php include('../layout/footer.php'); ?>
    <!--END FOOTER -->



    <!-- GLOBAL SCRIPTS -->
    <script src="../../assets/plugins/jquery-2.0.3.min.js"></script>
    <!-- <script src="assets/plugins/jquery-2.0.3.min.js"></script> -->
    <script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="../../assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
    <!-- END GLOBAL SCRIPTS -->
    <!-- PAGE LEVEL SCRIPTS -->
    <script src="../../assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->

</html>