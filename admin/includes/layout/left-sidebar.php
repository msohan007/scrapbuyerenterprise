<!-- MENU SECTION -->
<div id="left">
    <div class="media user-media well-small">
        <a class="user-link" href="#">
            <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif" />
        </a>
        <br />
        <div class="media-body">
            <h5 class="media-heading"><?php echo "<h4>" . $_SESSION['user_name'] . "</h4>" ?></h5>
            <ul class="list-unstyled user-info">

                <li>
                    <a class="btn btn-success btn-xs btn-circle" style="width: 10px;height: 12px;"></a> Online

                </li>

            </ul>
        </div>
        <br />
    </div>

    <ul id="menu" class="collapse">

        <li class="panel active">
            <a href="dashboard.php">
                <i class="icon-table"></i> Dashboard

            </a>
        </li>

        <li class="panel ">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                <i class="icon-user"> </i> User Management

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-default">10</span>&nbsp;
            </a>
            <ul class="collapse" id="component-nav">

                <li class=""><a  href="/admin/includes/user_management/register_request.php"><i class="icon-angle-right"></i> Register Requests &nbsp; <span class="label label-primary">10</span>&nbsp; </a>
                    <!-- <script>
                        function load_user_Request() {
                            //document.getElementById("display").innerHTML = '<object type="type/html" data="user-management.php" ></object>';
                            document.getElementById("display").innerHTML = '<object type="text/html" data="user-management.php" style="width:100%;height:100vh" ></object>';
                        }
                    </script> -->

                </li>
                <li class=""><a href="/admin/includes/user_management/employee.php"><i class="icon-angle-right"></i> Employees &nbsp; <span class="label label-info">6</span>&nbsp; </a></li>
                <li class=""><a href="/admin/includes/user_management/scrap_buyer.php"><i class="icon-angle-right"></i> Buyers &nbsp; <span class="label label-info">6</span>&nbsp; </a></li>
                <li class=""><a href="/admin/includes/user_management/scrap_seller.php"><i class="icon-angle-right"></i> Sellers &nbsp; <span class="label label-info">6</span>&nbsp; </a></li>
                <li class=""><a href="tabs_panels.html"><i class="icon-angle-right"></i> Transporter &nbsp; <span class="label label-info">6</span>&nbsp; </a></li>

            </ul>
        </li>
        <li class="panel ">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#form-nav">
                <i class="icon-edit"></i> Prod. Management
                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-success">5</span>&nbsp;
            </a>
            <ul class="collapse" id="form-nav">
                <li class=""><a href="../products/scrap-products.php"><i class="icon-angle-right"></i> Products List </a></li>
                <li class=""><a onclick="addProduct()" href="#"><i class="icon-angle-right"></i> Add Product </a>
                    <script>
                        function addProduct() {
                            //document.getElementById("display").innerHTML = '<object type="type/html" data="user-management.php" ></object>';
                            document.getElementById("display").innerHTML = '<object type="text/html" data="./products/add-product.php" style="width:100%;height:100vh" ></object>';
                        }
                    </script>
                </li>
                <li class=""><a href="../products/update-product.php"><i class="icon-angle-right"></i> Update Product </a></li>
                <li class=""><a href="forms_validation.html"><i class="icon-angle-right"></i> Remove Product </a></li>
                <li class=""><a href="forms_fileupload.html"><i class="icon-angle-right"></i> FileUpload </a></li>
                <!-- <li class=""><a href="forms_editors.html"><i class="icon-angle-right"></i> WYSIWYG / Editor </a></li> -->
            </ul>
        </li>

        <li class="panel">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pagesr-nav">
                <i class="icon-table"></i> Transactions

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-info">6</span>&nbsp;
            </a>
            <ul class="collapse" id="pagesr-nav">
                <li><a href="pages_calendar.html"><i class="icon-angle-right"></i> Calendar </a></li>
                <li><a href="pages_timeline.html"><i class="icon-angle-right"></i> Timeline </a></li>
                <li><a href="pages_social.html"><i class="icon-angle-right"></i> Social </a></li>
                <li><a href="pages_pricing.html"><i class="icon-angle-right"></i> Pricing </a></li>
                <li><a href="pages_offline.html"><i class="icon-angle-right"></i> Offline </a></li>
                <li><a href="pages_uc.html"><i class="icon-angle-right"></i> Under Construction </a></li>
            </ul>
        </li>
        <li class="panel">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav">
                <i class="icon-bar-chart"></i> Task Management

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-danger">4</span>&nbsp;
            </a>
            <ul class="collapse" id="chart-nav">
                <li><a href="charts_line.html"><i class="icon-angle-right"></i> Line Charts </a></li>
                <li><a href="charts_bar.html"><i class="icon-angle-right"></i> Bar Charts</a></li>
                <li><a href="charts_pie.html"><i class="icon-angle-right"></i> Pie Charts </a></li>
                <li><a href="charts_other.html"><i class="icon-angle-right"></i> other Charts </a></li>
            </ul>
        </li>


        <li><a href="gallery.html"><i class="icon-money"></i> Expense Management </a></li>
        <li><a href="tables.html"><i class="icon-money"></i> Profit </a></li>
        <li><a href="maps.html"><i class="icon-map-marker"></i> Cutomer Locations </a></li>

        <li><a href="grid.html"><i class="icon-info"></i> Query Messages </a></li>
        <li class="panel">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#blank-nav">
                <i class="icon-check-empty"></i> Profile Settings

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-success">2</span>&nbsp;
            </a>
            <ul class="collapse" id="blank-nav">

                <li><a href="blank.html"><i class="icon-angle-right"></i> Blank Page One </a></li>
                <li><a href="blank2.html"><i class="icon-angle-right"></i> Blank Page Two </a></li>
            </ul>
        </li>
        <li><a href="../../logout.php"><i class="icon-signout"></i> Logout </a></li>

    </ul>

</div>
<!--END MENU SECTION -->